Project proposal

which specifically technologies, libraries, frameworks, languages, etc, you intend to use or investigate using for your project
languages: python R++ probably R
technologies: raspberry pi /computer(mac and wimdows)
framworks: strategy studio /google colab
libraries: pandas pickle, numpy, keras, tensorflow, sklearn, matplotlib, itertools, pyfolio, backtrader ,datetime, seaborn, 

what specific hardware your project will need (are you running on VMs locally on your laptops, do you require use of the lab machines and bare metal, raspberry pis, AWS, etc).
raspberry pis, AWS

what frameworks, libraries, etc, you will use for CI/CD (automated testing, deployment, etc)
Automated testing: jupyter notebook/ google colab
Deployment：strategy studio/visual studio

if your project has any user interface aspects (GUIs, webpages, etc), a brief mockup of what you expect the UI to look like. 
No UI will be made since the project is about database

detailed timeline of what needs to be done and when to complete your project
to begin with, we will deploy

in the fist part we will get the data from professor and deploy the strategy studio and find connection between strategy studio and onetick.
bring up OneTick on a VM located in the lab (using DevOps)
Utilize existing market data parsers to load up market data into OneTick
Develop or locate other market data parsers and fetch scripts to load additional sources of data into the OneTick instance
Integrate OneTick database with our backtesting software (StrategyStudio - it already supports it)
Develop various example tutorials and scripts for utilzing their API (pulling various amounts of data with python, loading into pandas, visualizing with various plotting libraries like matplotlib, etc)
Use other database for the same process and try to compare the performance between them.

minimum target for your project to not be a failure
try to store the tick data into the onetick and write detailed script for the usage of onetick.
expected completion goals, and "Reach" goals (more advanced work that, given enough time, your team hopes to also accomplish). 
Compare the database with the existed ones, write the comparison report of these databases.
detailed specification of what each individual team member will be contributing to the project (and ensuring this aligns with everyone's actual skills or ability to learn new skills). 
Xi teng will responsible for deployment of strategy studio.
Xiaoyue chen is responsible for the data part
Luwei wang and chenling miao is responsible for the onetick and test of hardware
after that we will write a description for the onetick together.
any external resources your project may depend on or require (including anything you need from me). 
The access to onetick and tick datas.
What are the final deliverable(s) of your project. 
Develop various example tutorials and scripts for utilzing their API (pulling various amounts of data with python, loading into pandas, visualizing with various plotting libraries like matplotlib, etc

Graduation date for the team.
Chenling miao 2024/12
Xi teng: 2024/12
Xiaoyue chen: 2024/05 
Luwei wang 2025/05

